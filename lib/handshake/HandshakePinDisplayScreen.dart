import 'package:flutter/material.dart';
import 'package:share_otp/network/ProjectRepository.dart';

class HandshakePinDisplayScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HandshakePinDisplayScreenState();
  }
}

class HandshakePinDisplayScreenState extends State<HandshakePinDisplayScreen> {
  ProjectRepository projectRepository = new ProjectRepository();

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text("Pairing"),
        ),
        body: FutureBuilder<String>(
            future: projectRepository.generatePin(),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.done) {
                if (snapshot.hasError) {
                  return Center(child: Text("Some error occured"));
                }

                return Center(
                    child: Text(
                        'Please share this pin for pairing : ${snapshot.data}'));
              } else
                return Center(child: CircularProgressIndicator());
            }));
  }
}
