import 'package:flutter/material.dart';
import 'package:share_otp/network/ProjectRepository.dart';

class HandshakePinInputScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HandshakePinInputState();
  }
}

class HandshakePinInputState extends State<HandshakePinInputScreen> {
  TextEditingController pinController = new TextEditingController();
  ProjectRepository projectRepository = new ProjectRepository();

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text("Pairing"),
        ),
        body: new Container(child: new Builder(builder: (BuildContext context) {
          return new Column(
            children: [
              new Text(
                "Enter pin",
                style: TextStyle(fontSize: 30.0),
              ),
              new TextField(
                decoration: InputDecoration(
                    border: InputBorder.none, hintText: 'Enter your pin'),
                controller: pinController,
              ),
              new RaisedButton(
                  child: new Text("Submit"),
                  onPressed: () {
                    projectRepository
                        .confirmPin(int.parse(pinController.text))
                        .then((status) {
                      if (!status) {
                        Scaffold.of(context).showSnackBar(SnackBar(
                          content: Text("Sorry, Incorrect pin"),
                        ));
                      } else {
                        Navigator.of(context).pop();
                      }
                    });
                  })
            ],
          );
        })));
  }
}
