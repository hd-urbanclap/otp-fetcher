import 'package:flutter/material.dart';

import 'HandshakePinDisplayScreen.dart';
import 'HandshakePinInputScreen.dart';

class HandshakeStartScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HandshakeStartScreenState();
  }
}

class HandshakeStartScreenState extends State<HandshakeStartScreen> {
  void _handleCreatePinClick() {
    Navigator.push(
      context,
      new MaterialPageRoute(
          builder: (context) => new HandshakePinDisplayScreen()),
    );
  }

  void _handleEnterPinClick() {
    Navigator.push(
      context,
      new MaterialPageRoute(
          builder: (context) => new HandshakePinInputScreen()),
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Pairing"),
      ),
      body: Center(
        child: new Container(
          child: new Column(
              children: [
                new RaisedButton(
                    child: new Text("      Create PIN      ",
                        style: TextStyle(color: Colors.white)),
                    color: Colors.cyan,
                    onPressed: () {
                      _handleCreatePinClick();
                    }),
                new RaisedButton(
                    child: new Text("        Enter PIN        ",
                        style: TextStyle(color: Colors.white)),
                    color: Colors.cyan,
                    onPressed: () {
                      _handleEnterPinClick();
                    })
              ],
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center),
        ),
      ),
    );
  }
}
