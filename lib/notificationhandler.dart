import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:sms/sms.dart';

import 'network/ProjectRepository.dart';
import 'sms_helper.dart';

class NotificationManager {
  ProjectRepository projectRepository = new ProjectRepository();

  configureFcm(FirebaseMessaging _fcm, BuildContext context) {
    _fcm.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
        showDialog(
          context: context,
          builder: (context) => AlertDialog(
            content: ListTile(
              title: Text(message['notification']['title']),
              subtitle: Text(message['notification']['body']),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text('Ok'),
                onPressed: () => Navigator.of(context).pop(),
              ),
            ],
          ),
        );
        sendOtp(message['data']['fetcherUserId']);
      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
      },
    );
    _saveDeviceToken(_fcm);
  }

  /// Get the token, save it to the database for current user
  _saveDeviceToken(FirebaseMessaging _fcm) async {
    // Get the token for this device
    String fcmToken = await _fcm.getToken();
    // Save it to Firestore
    if (fcmToken != null) {
      FirebaseAuth.instance.currentUser().then((onValue) {
        if (onValue != null && onValue.uid != null && onValue.uid != "") {
          projectRepository.updateFcmToken(onValue.uid, fcmToken);
        }
      });
    }
  }

  sendOtp(String fetcherUserId) {

    ProjectRepository projectRepository = new ProjectRepository();


    SmsHelper smsHelper = SmsHelper();
    List<SmsMessage> filteredMessages;
    smsHelper.readSms().then((value) {
      // Run extra code here
      filteredMessages = value.where((i) => i.body.contains("OTP")).toList();

      SmsMessage lastMessage = filteredMessages[0];

      projectRepository.sendOtp(fetcherUserId,lastMessage.body);

    }, onError: (error) {
      print(error);
    });
  }
}
