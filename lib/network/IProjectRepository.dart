import 'dart:async';

import 'package:http/http.dart';

import 'NetworkModels/PairedUsersModel.dart';
import 'ProjectRepository.dart';

abstract class IProjectRepository {
//  Future saveFCMToken(String fcmToken);
//  Future fetchPairedUsers();
//  Future generateHandshakePin();
//  Future confirmHandShakePin(String pin);
//  Future fetchOtp(String userId);
//  Future fetchHistory(String userId);
//  Future sendOtp(String message, String receiverUserId);
  Future createUser(String userId, String fcmToken, String userName);
  Future updateFcmToken(String userId, String fcmToken);
  StreamController<PairedUsersModel> getPairedUsers(String userId);
  Future<String> generatePin();
  Future<bool> confirmPin(int pin);
  Future<bool> fetchOtp(String userId);
  Future<bool> sendOtp(String userId, String message);
//  void getPairedUsers(String userId);

//  Future<Post> getDummyData();
}