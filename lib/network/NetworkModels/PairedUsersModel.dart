class PairedUsersModel {
  final String name;
  final String userId;

  PairedUsersModel({this.name, this.userId});

}