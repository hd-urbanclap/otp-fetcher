import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cloud_functions/cloud_functions.dart';
import 'package:share_otp/network/NetworkModels/PairedUsersModel.dart';

import 'IProjectRepository.dart';

class ProjectRepository  implements IProjectRepository {
  @override
  Future createUser(String userId, String fcmToken, String userName) {
    return Firestore.instance.collection('users').document(userId)
        .setData({ 'user_id': '$userId', 'user_name': '$userName', 'fcm_id':fcmToken});
  }

  @override
  Future updateFcmToken(String userId, String fcmToken) {
    return Firestore.instance.collection('users').document(userId).updateData({"fcm_id": fcmToken});
  }

  @override
  StreamController<PairedUsersModel> getPairedUsers(String userId) {
    StreamController<PairedUsersModel> streamController = StreamController();
    Firestore.instance.collection("paired_users").document(userId).get().then((DocumentSnapshot snapshot){
       List<String> pairedUserIdList = snapshot.data["users"].cast<String>();
       for (userId in pairedUserIdList) {
         Firestore.instance.collection('users').document(userId).get().then((DocumentSnapshot user){
           streamController.add(PairedUsersModel(name: user.data["user_name"], userId: userId));
         });
       }
    });
    return streamController;
  }

  @override
  Future<String> generatePin() async {
    final HttpsCallable callable = CloudFunctions.instance.getHttpsCallable(
      functionName: 'generatePin',
    );
    try {
      HttpsCallableResult result = await callable.call();
      return result.data["pin"].toString();
    } catch(e) {
      rethrow;
    }
  }

  @override
  Future<bool> confirmPin(int pin) async {
    final HttpsCallable callable = CloudFunctions.instance.getHttpsCallable(
      functionName: 'confirmPin',
    );
    try {
      HttpsCallableResult result = await callable.call({'pin':pin});
      return result.data["status"];
    } catch(e) {
      rethrow;
    }
  }

  @override
  Future<bool> fetchOtp(String userId) async {
    final HttpsCallable callable = CloudFunctions.instance.getHttpsCallable(
      functionName: 'fetchOtp',
    );
    try {
      HttpsCallableResult result = await callable.call({'user_id':userId});
      return result.data["status"];
    } catch(e) {
      rethrow;
    }
  }

  @override
  Future<bool> sendOtp(String userId, String message) async {
    final HttpsCallable callable = CloudFunctions.instance.getHttpsCallable(
      functionName: 'sendOtp',
    );
    try {
      HttpsCallableResult result = await callable.call({'user_id':userId, 'message':message});
      return result.data["status"];
    } catch(e) {
      rethrow;
    }
  }

}
