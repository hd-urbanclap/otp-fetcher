import 'package:sms/sms.dart';

class SmsHelper {
  SmsQuery query = new SmsQuery();


  Future<List<SmsMessage>> readSms()  async {
    List<SmsMessage> messages;
    try {
      messages =  await query.querySms(kinds: [SmsQueryKind.Inbox]);
    } on Exception {
      throw Future.error('Error from return');
    }
    return messages;
  }}