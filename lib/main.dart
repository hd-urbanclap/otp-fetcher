import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:share_otp/network/NetworkModels/PairedUsersModel.dart';
import 'package:share_otp/network/ProjectRepository.dart';

import 'OnboardingScreen.dart';
import 'handshake/HandshakeStartScreen.dart';
import 'loginmanager.dart';
import 'notificationhandler.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: SplashPage(),
    );
  }
}

class SplashPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SplashState();
  }



}

class SplashState extends State<SplashPage> {

  @override
  void initState() {
    var firebaseAuth = FirebaseAuth.instance;
    firebaseAuth.currentUser().then((FirebaseUser user) {
        if (user == null) {
          Navigator.pushReplacement(
            context,
            new MaterialPageRoute(
                builder: (context) => new OnboardingScreen()),
          );
        } else {
          Navigator.pushReplacement(
            context,
            new MaterialPageRoute(
                builder: (context) => new MyHomePage(title: 'Share Otp')),
          );
        }
    });
//    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container();
  }

}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();


}

class _MyHomePageState extends State<MyHomePage> {
  String _login_text = "...";
  List<PairedUsersModel> pairedUsers = new List<PairedUsersModel>();
  ProjectRepository projectRepository = new ProjectRepository();
  final FirebaseMessaging _fcm = FirebaseMessaging();

  @override
  void initState() {

    var firebaseAuth = FirebaseAuth.instance;
    firebaseAuth.currentUser().then((FirebaseUser user) {
      setState(() {
        if (user == null) {
          setState(() {
            _login_text = "Not logged in";
          });
        } else {
          _login_text = "Logged in as " + user.phoneNumber;
        }
      });
    });

    FirebaseAuth.instance.currentUser().then((FirebaseUser user) {
      projectRepository
          .getPairedUsers(user.uid)
          .stream
          .listen((PairedUsersModel pairedUsersModel) {
        setState(() {
          pairedUsers.add(pairedUsersModel);
        });
      });
    });


    initNotification(_fcm,context);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: ListView.builder(
        itemCount: pairedUsers.length,
        itemBuilder: (context, index) => this._buildRow(index),
      ),
      floatingActionButton: FloatingActionButton(
        // onPressed: _doIt,
        onPressed: () {
          Navigator.push(
            context,
            new MaterialPageRoute(
                builder: (context) => new HandshakeStartScreen()),
          );
        },
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),
    );
  }

  _buildRow(int index) {
      // return Text("  " + pairedUsers[index-1]);
      return Container(
        padding: const EdgeInsets.only(left: 16, right: 16, top: 16, bottom: 8),
        child: new MaterialButton(
          child: new Text("  " + pairedUsers[index].name),
          onPressed: () {
            projectRepository.fetchOtp(pairedUsers[index].userId).then((status) {
              if(status) {
                Scaffold.of(context).showSnackBar(SnackBar(
                  content: Text("Sorry, Unable to contact server"),
                ));
              } else {
                Scaffold.of(context).showSnackBar(SnackBar(
                  content: Text("Fetching OTP, Please wait..."),
                ));
              }

            })
            .catchError((onError){
              Scaffold.of(context).showSnackBar(SnackBar(
                content: Text("Sorry, Unable to contact server"),
              ));
            });
          },
            ),
      );
  }

  void initNotification(fcm, BuildContext context) {
    NotificationManager messageHandler = NotificationManager();
    messageHandler.configureFcm(fcm, context);

  }
}
