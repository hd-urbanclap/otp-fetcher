import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';

import 'main.dart';
import 'network/ProjectRepository.dart';

class PhoneNumberScreen extends StatelessWidget {
  BuildContext context;

  TextEditingController nameTextFieldController = new TextEditingController();
  TextEditingController otpTextFieldController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    this.context = context;

    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Enter your Phone number"),
      ),
      body: new Container(
        child: new Column(
          children: [
            new Text(
              "Phone Number",
              style: TextStyle(fontSize: 30.0),
            ),
            new TextField(
              decoration: InputDecoration(
                  border: InputBorder.none, hintText: 'Enter your name'),
              controller: nameTextFieldController,
            ),
            new TextField(
              decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: 'Enter your phone number'),
              keyboardType: TextInputType.number,
              controller: otpTextFieldController,
            ),
            new RaisedButton(
                child: new Text("NEXT"),
                onPressed: () {
                  _doIt("+91" + otpTextFieldController.text,
                      nameTextFieldController.text);
                })
          ],
        ),
      ),
    );
  }

  void _doIt(String phoneNumber, String name) async {
    _handleSignIn(phoneNumber, name)
        .then((FirebaseUser user) => print(user))
        .catchError((e) => print(e));

//    final FirebaseUser user = (await _auth.createUserWithEmailAndPassword(
//    email: 'an email',
//    password: 'a password',
//    )).user;
  }

  Future<FirebaseUser> _handleSignIn(String phoneNumber, String name) async {
//    final GoogleSignInAccount googleUser = await _googleSignIn.signIn();
//    final GoogleSignInAuthentication googleAuth = await googleUser.authentication;
//    final AuthCredential credential = GoogleAuthProvider.getCredential(
//      accessToken: googleAuth.accessToken,
//      idToken: googleAuth.idToken,
//    );

    var firebaseAuth = FirebaseAuth.instance;

    final PhoneCodeSent codeSent =
        (String verificationId, [int forceResendingToken]) {
      Navigator.push(
        context,
        new MaterialPageRoute(
            builder: (context) => new PhoneNumberOtpScreen(
              phoneNumber: phoneNumber,
              name: name,
              actualCode: verificationId,
            )),
      );
    };

    final PhoneCodeAutoRetrievalTimeout codeAutoRetrievalTimeout =
        (String verificationId) {
      Navigator.push(
        context,
        new MaterialPageRoute(
            builder: (context) => new PhoneNumberOtpScreen(
              phoneNumber: phoneNumber,
              name: name,
              actualCode: verificationId,
            )),
      );
    };

    final PhoneVerificationFailed verificationFailed =
        (AuthException authException) {};

    final PhoneVerificationCompleted verificationCompleted =
        (AuthCredential auth) {
      firebaseAuth.signInWithCredential(auth).then((AuthResult value) {
        if (value.user != null) {
          // DONEE
          Navigator.of(context).pushAndRemoveUntil(
              MaterialPageRoute(
                  builder: (context) => MyHomePage(title: 'Share Otp')
              ),
                  (Route<dynamic> route) => false
          );

          ProjectRepository projectRepository = new ProjectRepository();

          FirebaseMessaging _fcm = FirebaseMessaging();
          var gcmToken = _fcm.getToken().then((onValue) {
            projectRepository.createUser(value.user.uid, onValue, name);
          });
        } else {}
      }).catchError((error) {});
    };

    firebaseAuth.verifyPhoneNumber(
        phoneNumber: phoneNumber,
        timeout: Duration(seconds: 60),
        verificationCompleted: verificationCompleted,
        verificationFailed: verificationFailed,
        codeSent: codeSent,
        codeAutoRetrievalTimeout: codeAutoRetrievalTimeout);

    /////////////////////////////////////////////////////////////////////////////////////////////////

//    final GoogleSignInAccount googleUser = await _googleSignIn.signIn();
//    final GoogleSignInAuthentication googleAuth = await googleUser.authentication;

//    final AuthCredential credential = GoogleAuthProvider.getCredential(
//      accessToken: googleAuth.accessToken,
//      idToken: googleAuth.idToken,
//    );

//    final FirebaseUser user = (await _auth.signInWithCredential(credential)).user;
//    print("signed in " + user.displayName);
//    return user;
  }
}

class PhoneNumberOtpScreen extends StatelessWidget {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  final String phoneNumber;
  final String name;

  BuildContext context;

  String actualCode;

  TextEditingController textFieldController = new TextEditingController();

  PhoneNumberOtpScreen(
      {Key key,
        @required this.phoneNumber,
        @required this.name,
        @required this.actualCode})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    this.context = context;

    return new Scaffold(
      key: _scaffoldKey,
      appBar: new AppBar(
        title: new Text("Enter OTP"),
      ),
      body: new Container(
        child: new Column(
          children: [
            new Text(phoneNumber),
            new TextField(
              decoration: InputDecoration(
                  border: InputBorder.none, hintText: 'Enter your OTP'),
              keyboardType: TextInputType.number,
              controller: textFieldController,
            ),
            new RaisedButton(
                child: new Text("CHECK"),
                onPressed: () {
                  _signInWithPhoneNumber(textFieldController.text);
                })
          ],
        ),
      ),
    );
  }

  void _signInWithPhoneNumber(String smsCode) async {
    var authCredential = await PhoneAuthProvider.getCredential(
        verificationId: actualCode, smsCode: smsCode);
    FirebaseAuth.instance.signInWithCredential(authCredential).catchError((onError) {
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text("Sorry, Incorrect OTP"),
      ));
    }).then((onValue){
      var user = onValue.user;
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(
              builder: (context) => MyHomePage(title: 'Share Otp')
          ),
              (Route<dynamic> route) => false
      );

      ProjectRepository projectRepository = new ProjectRepository();
      projectRepository.createUser(user.uid, "", name);
    });
    // DONEE

  }
}
