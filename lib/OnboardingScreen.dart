import 'package:flutter/material.dart';
import 'package:share_otp/network/ProjectRepository.dart';

import 'loginmanager.dart';

class OnboardingScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(32),
      color: Colors.white,
      child: Container(
        padding: const EdgeInsets.all(32),
        child: Column(
            children:[
              Image.asset(
                'images/boot.png',
                fit: BoxFit.contain,
              ),
              Container(
                child: RaisedButton(
                  textColor: Colors.white,
                  color: Colors.black,
                  child: Text("Login/Signup"),
                  onPressed: () {
                    Navigator.push(
                      context,
                      new MaterialPageRoute(
                          builder: (context) => new PhoneNumberScreen()),
                    );
                  },
                ),
                margin: EdgeInsets.fromLTRB(0, 48, 0, 0),
              )
            ]
        ),
      ) ,
    );
  }
}